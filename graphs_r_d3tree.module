<?php

/**
 *
 */
function graphs_r_d3tree_graphs_plugin_info() {
  return array(
    'renderers' => array(
      'd3tree' => array(
        'module' => 'graphs_r_d3tree',
        'version' => '1.0.0',
        'compatibility' => array(
          'sourcers' => array(
            'default' => TRUE,
          ),
          'renderers' => array(
            'default' => FALSE,
          ),
          'actions' => array(
            'default' => TRUE,
          ),
        ),
      ),
    ),
  );
}

/**
 *
 */
function graphs_r_d3tree_graphs_form($graph_renderer) {
  return array(
    '#type'=> 'fieldset',
    '#collapsible' => FALSE,
    '#collapsed'   => FALSE,
    '#title'       => t('D3JS Tree renderer options'),
    '#attributes' => array(
      'class' => array(
        'graph-renderer-type-d3tree',
      ),
    ),

    'tree_title' => array(
      '#title' => t('Tree title'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_renderer->tree_title) ? $graph_renderer->tree_title : '',
      '#description' => t('Title of the tree.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'parent_of_relationships' => array(
      '#title' => t('Type name of the parental relationships (coma-separated list)'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_renderer->parent_of_relationships) ? $graph_renderer->parent_of_relationships : '',
      '#description' => t('If you use special relationship types, enter the name of the relationships that defines a node as a parent of another node (ex. "parent_of"). You should leave this field empty if you don\'t use special relationship types.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'child_of_relationships' => array(
      '#title' => t('Type name of the childhood relationship (coma-separated list)'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_renderer->child_of_relationships) ? $graph_renderer->child_of_relationships : '',
      '#description' => t('If you use special relationship types, enter the name of the relationship that defines a node as a child of another node (ex. "child_of"). You should leave this field empty if you don\'t use special relationship types.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'root' => array(
      '#title' => t('Re-root tree on this node'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_renderer->root) ? $graph_renderer->root : '',
      '#description' => t('Re-root tree on the specified node identifier.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'default_depth' => array(
      '#title' => t('Default tree depth to load'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_renderer->default_depth) ? $graph_renderer->default_depth : '',
      '#description' => t('Tree depth to load by default. Must be a positive integer.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'zoom_enabled' => array(
      '#title' => t('Enable zoom'),
      '#type' => 'checkbox',
      '#default_value' => isset($graph_renderer->zoom_enabled) ? $graph_renderer->zoom_enabled : 1,
      '#description' => t('Enables or disables zoom feature.'),
      '#required' => FALSE,
    ),

    'default_zoom' => array(
      '#title' => t('Default zoom level'),
      '#type' => 'textfield',
      '#default_value' => isset($graph_renderer->default_zoom) ? $graph_renderer->default_zoom : '',
      '#description' => t('Default zoom value. Must be a number.'),
      '#required' => FALSE,
      '#size' => 30,
    ),

    'pan_enabled' => array(
      '#title' => t('Enable panning'),
      '#type' => 'checkbox',
      '#default_value' => isset($graph_renderer->pan_enabled) ? $graph_renderer->pan_enabled : 1,
      '#description' => t('Enables or disables panning feature.'),
      '#required' => FALSE,
    ),

    'layout' => array(
      '#title' => t('Layout type'),
      '#type' => 'radios',
      '#default_value' => isset($graph_renderer->layout) ? $graph_renderer->layout : 'htree',
      '#options' => array(
        'htree' => t('Horizontal tree'),
        'vtree' => t('Vertical tree'),
        'radial' => t('Radial tree'),
      ),
      '#description' => t('Selects the tree layout.'),
      '#required' => FALSE,
    ),

    'branch_length' => array(
      '#title' => t('Branch length'),
      '#type' => 'radios',
      '#default_value' => isset($graph_renderer->branch_length) ? $graph_renderer->branch_length : 'fixed',
      '#options' => array(
        'fixed' => t('Fixed branch length.'),
        'data' => t('Specified by source data.'),
        'dendogram' => t('Align leaves.'),
      ),
      '#description' => t('Selects how to handle branch length.'),
      '#required' => FALSE,
    ),

    'branch_type' => array(
      '#title' => t('Branch type'),
      '#type' => 'radios',
      '#default_value' => isset($graph_renderer->branch_type) ? $graph_renderer->branch_type : 'curved',
      '#options' => array(
        'curved' => t('Curved branches.'),
        'straight' => t('Straight branches.'),
        'square' => t('Square branches.'),
      ),
      '#description' => t('Selects how to display branches.'),
      '#required' => FALSE,
    ),

  );
}

/**
 *
 */
/*
@code
function graphs_r_d3tree_graphs_data_sourcer_form_validate($form, $form_state) {
  $d3tree_parameters = $form['graphs_r_d3tree']['parameters']['d3tree'];
}
@endcode
*/

/**
 *
 */
function graphs_r_d3tree_graphs_render($graph_renderer) {
  $render_array = array();

  $d3js_library = libraries_detect('d3js4');
  if (!$d3js_library) {
    drupal_set_message(
      t('Failed to load D3 JS v4 library: library not found.'),
      'error'
    );
  }
  elseif (empty($d3js_library['installed'])) {
    drupal_set_message(
      t(
        "Failed to load D3 JS v4 library: !error<br/>\n!error_message",
        array(
          '!error' => $d3js_library['error'],
          '!error_message' => $d3js_library['error message'],
        )
      ),
      'error'
    );
  }
  else {
    libraries_load('d3js4');

    // Adds rendering settings.
    drupal_add_js(
      array(
        'graphs' => array(
          'renderers' => array(
            $graph_renderer->id => array(
              'type' => $graph_renderer->type,
              'module' => 'graphs_r_d3tree',
              // Parameters.
              'branch_length' => $graph_renderer->branch_length,
              'branch_type' => $graph_renderer->branch_type,
              'default_depth' => intval($graph_renderer->default_depth),
              'default_zoom' => $graph_renderer->default_zoom,
              'layout' => $graph_renderer->layout,
              'pan_enabled' => $graph_renderer->pan_enabled,
              'parent_of_relationships' => preg_split('/\s*,\s*/', $graph_renderer->parent_of_relationships),
              'child_of_relationships' => preg_split('/\s*,\s*/', $graph_renderer->child_of_relationships),
              'root' => $graph_renderer->root,
              'tree_title' => $graph_renderer->tree_title,
              'zoom_enabled' => $graph_renderer->zoom_enabled,
            ),
          ),
        ),
      ),
      array('type' => 'setting')
    );

    // Adds Javascript code for rendering.
    $render_array['#attached']['js'][] = array(
      'type' => 'file',
      'data' => drupal_get_path('module', 'graphs_r_d3tree') . '/graphs_r_d3tree.js',
    );

    $render_array['#attached']['css'][] =
      drupal_get_path('module', 'graphs_r_d3tree') . '/graphs_r_d3tree.css';
  }

  return $render_array;
}
