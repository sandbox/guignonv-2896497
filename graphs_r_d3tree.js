/**
 * @file
 * Graphs Javascript library.
 */

(function ($) {

Drupal.behaviors.graphs_r_d3tree = {
  attach: function (context, settings) { },

  detach: function (context, settings) { },

  render: function (graph_index, rdr_index) {
    // Main (global) variables.
    var graph = Drupal.settings.graphs.graphs[graph_index];
    var renderer = Drupal.settings.graphs.renderers[rdr_index];
    var sourcer = Drupal.settings.graphs.sourcers[graph.sourcer];
    
    // Initialization stuff:
    // - create main SVG element;
    $('#graphs_' + graph_index)
      .append(
        '<svg class="graphs_r_d3tree" width="'
        + (graph.width ? graph.width : '100%')
        + '" height="'
        + (graph.height ? graph.height : '500')
        + '"></svg>');
    // - display tree title.
    if (renderer.tree_title) {
      $('#graphs_' + graph_index)
        .prepend("<h2>" + renderer.tree_title + "</h2>\n");
    }

    // Global render variables:
    // @todo: replace some fixed values by dynamic ones.
    // - tree layout.
    var tree;
    // - tree nodes.
    var tree_data = {};
    // - SVG area.
    var svg = d3.select('#graphs_' + graph_index + " svg");
    // - root SVG group element.
    var g = svg.append("g");
    // - drawing area padding.
    var display_margin = 50;
    // - dimension of the rendering area.
    var viewer_width = +svg.attr("width");
    var viewer_height = +svg.attr("height");
    // - transistion duration.
    var duration = 750;
    // - zoom manager.
    var zoom_listener = d3.zoom()
      .scaleExtent([
        (renderer.zoom_enabled ? 0.25 : 1),
        (renderer.zoom_enabled ? 50 : 1)
      ])
      .translateExtent(
        renderer.pan_enabled ?
        [[-200, -200], [viewer_width + 200, viewer_height + 200]]
        : [[0, 0], [viewer_width, viewer_height]]
      )
      .on("zoom", zoomHandler);



    // Render sub-functions.
    /**
     * Converts JSON data from the sourcer into a structure for D3.
     */
    function convertSourceToTree(source) {
      var parent_of_regex = new RegExp(
        '^(?:parent_of'
        + (renderer.parent_of_relationships.length ?
          '|' + renderer.parent_of_relationships.join('|')
          : '')
        + ')$',
        'i'
      );
      var child_of_regex = new RegExp(
        '^(?:child_of'
        + (renderer.child_of_relationships.length ?
          '|' + renderer.child_of_relationships.join('|')
          : '')
        + ')$',
        'i'
      );

      var children_of = {};
      // Inititalizes lookup array.
      source.nodes.forEach(function (node) {
        children_of[node.id] = [];
      });
      // Fills lookup array.
      source.links.forEach(function (link) {
        if (!link.relationship || (link.relationship.match(parent_of_regex))) {
          children_of[link.source].push(link.target);
        }
        else if (link.relationship.match(child_of_regex)) {
          children_of[link.target].push(link.source);
        }
      });
      
      // Build parental relationships.
      var parental_relationship = {};
      function buildParentalRelationship(node_id) {
        children_of[node_id].forEach(function (child_id) {
          parental_relationship[child_id] = node_id;
          buildParentalRelationship(child_id);
        });
      }
      
      buildParentalRelationship(source.nodes[0].id);
      var nodes = [source.nodes[0]];

      // Only keep nodes with parents.
      source.nodes.forEach(function (node) {
        if (null != parental_relationship[node.id]) {
          nodes.push(node);
        }
      });

      return d3.stratify()
        .id(function (d) { return d.id; })
        .parentId(function (d) {
          return parental_relationship[d.id];
        })
      (nodes);
    }

    /**
     * Compute tree height, width and depth.
     *
     * The height is the vertical distance between the given node and is
     * farest child (for a vertical tree) which take into account branch
     * length from parents to children.
     *
     * The width is the maximum number of column with nodes (for a vertical
     * tree).
     *
     * The depth is the maximum number of nodes that can be met between the
     * given node and its subtree leaves.
     */
    function computeTreeDimensionsAt(node) {
      var height = 0, width = 0, depth = 0;
      if (node.relationships['parent_of']) {
        node.relationships['parent_of'].forEach(function (link) {
          var subtree_dim = computeTreeDimensionsAt(sourcer.nodes[link.target]);
          height = Math.max(
            height,
            (link.branch_length ? link.branch_length : 1)
            + subtree_dim.height
          );
          width += Math.max(1, subtree_dim.width);
          depth = Math.max(depth, 1 + subtree_dim.depth);
        });
      }
      return {'height': height, 'width': width, 'depth': depth};
    }

    /**
     * Loads additional child nodes at a given position.
     */
    function loadNodes(d) {
      // Get children.
      var children = Drupal.behaviors.graphs.getGraph(
        graph_index,
        graph.sourcer,
        {
          graph_id: graph.id,
          node_id: d.data.id,
          depth: (renderer.default_depth ? renderer.default_depth : 1),
          rel_types: (renderer.parent_of_relationships ? renderer.parent_of_relationships : ['parent_of']),
          back_rel_types: (renderer.child_of_relationships ? renderer.child_of_relationships : ['child_of']),
          callback: function (subgraph_data) {
            tree_data = convertSourceToTree(sourcer.graph);

            // Update display zone if branch length can be specified by data.
            if ('data' == renderer.branch_length) {
              if (renderer.pan_enabled) {
                var tree_dimensions = computeTreeDimensionsAt(tree_data.data);
                if ('vtree' == renderer.layout) {
                  zoom_listener
                    .translateExtent(
                      [[-200, -200], [viewer_width + 200, (viewer_height*tree_dimensions.height/tree_dimensions.depth) + 200]]
                    );
                }
                else if ('htree' == renderer.layout) {
                  zoom_listener
                    .translateExtent(
                      [[-200, -200], [(viewer_width*tree_dimensions.height/tree_dimensions.depth) + 200, viewer_height + 200]]
                    );
                }
                else if ('radial' == renderer.layout) {
                  zoom_listener
                    .translateExtent(
                      [[-200, -200], [(viewer_width*tree_dimensions.height/tree_dimensions.depth) + 200, (viewer_height*tree_dimensions.height/tree_dimensions.depth) + 200]]
                    );
                }
              }
            }
            updateSubTree(d);
          }
        }
      );
    }


    /**
     * Collapses all descendant nodes of a given node.
     */
    function collapseSubNodes(d, all_nodes = true) {
      if (d.children) {
        d._children = d.children;
        if (all_nodes) {
          d._children.forEach(collapse);
        }
        d.children = null;
      }
    }

    /**
     * Expands all descendant nodes of a given node.
     */
    function expandSubNodes(d, all_nodes = true) {
      if (d._children) {
        d.children = d._children;
        if (all_nodes) {
          d.children.forEach(expand);
        }
        d._children = null;
      }
    }

    /**
     * Expands all parent nodes up to a given node.
     */
    function expandTo(d, id) {
      var id_in_subtree = false;
      if (d.children) {
        d.children.forEach(function (e) {
          id_in_subtree = id_in_subtree || expandTo(e, id);
        });
      }
      else if (d._children) {
        d._children.forEach(function (e) {
          id_in_subtree = id_in_subtree || expandTo(e, id);
        });
        if ((id_in_subtree) || (id == d.id)) {
          d.children = d._children;
          d._children = null;
        }
      }
      return id_in_subtree || ((id == d.id) ? d : false);
    }

    /**
     * Display or hide a subtree rooted on the given node.
     */
    function toggleSubTree(d) {
      if (d.children) {
        d._children = d.children;
        d.children = null;
      }
      else if (d._children) {
        d.children = d._children;
        d._children = null;
      }
      return d;
    }

    /**
     * Center tree on a given node.
     */
    function centerNode(d) {
      var transform = d3.zoomTransform(d);
      transform.x = -d.y0 * transform.k + viewer_width / 2;
      transform.y = -d.x0 * transform.k + viewer_height / 2;
      svg.transition()
          .duration(duration)
          .attr("transform", transform);
    }

    /**
     * Handles click action on a node.
     */
    function clickNode(d) {
      // Prevent node change while node is currently being changed.
      if (d.on_transition) { return;}
      if (d3.event.defaultPrevented) return;

      if (d.children || d._children) {
        d = toggleSubTree(d);
        updateSubTree(d);
        // @todo: fix centering method
        // centerNode(d);
      }
      else if (!d.data.children_loaded) {
        loadNodes(d);
        d.data.children_loaded = true;
        d.data.classes += ' node--children-loaded';
      }
    }

    /**
     * Handles zooming and panning.
     */
    function zoomHandler() {
      if (!renderer.pan_enabled) {
        d3.event.transform.x = 0;
        d3.event.transform.y = 0;
      }
      g.attr("transform", d3.event.transform);
      // @todo: find a way to prevent scrolling when zoom reached its limits.
      // if (1 >= d3.event.transform.k) {
      //   d3.event.preventDefault();
      //   d3.event.stopPropagation();
      //   d3.event.stopImmediatePropagation();
      // }
    }

    /**
     * Reset tree display.
     */
    function resetTree() {
      svg.transition()
          .duration(duration)
          .call(zoom_listener.transform, d3.zoomIdentity);
    }

    /**
     *
     */
    // @todo: add zoom in/out/reset buttons.
    // @todo: add blury border to graph when zoomed.
    // @todo: add functions for sorting nodes.

    /**
     * Draw or re-draw a subtree at a given node.
     *
     * This is the main tree rendering function.
     */
    function updateSubTree(root_node) {
      var tree_nodes = tree(tree_data);

      // Compute the new tree layout.
      var nodes = tree_nodes.descendants();

      // Setup node branch length.
      // nodes.forEach(function(d) { d.y = d.depth * 200});

      function getXCoordinate(n, initial = false) {
        if ('htree' == renderer.layout) {
          if ('data' == renderer.branch_length
              && n.data
              && n.data.relationships
              && n.data.relationships['child_of'].length) {
            return n.data.relationships['child_of'][0].branch_length * (initial ? n.y0 : n.y);
          }
          else {
            return initial ? n.y0 : n.y;
          }
        }
        else {
          return initial ? n.x0 : n.x;
        }
      }

      function getYCoordinate(n, initial = false) {
        if ('htree' == renderer.layout) {
          return initial ? n.x0 : n.x;
        }
        else {
          if ('data' == renderer.branch_length
              && n.data
              && n.data.relationships
              && n.data.relationships['child_of'].length) {
            return n.data.relationships['child_of'][0].branch_length * (initial ? n.y0 : n.y);
          }
          else {
            return initial ? n.y0 : n.y;
          }
        }
      }

      function radialPoint(x, y) {
        return [(y = +y) * Math.cos(x -= Math.PI / 2), y * Math.sin(x)];
      }

      var node = g.selectAll('g.node')
          .data(nodes, function(d) { return d.id; });

      var node_enter = node.enter().append('g')
        .attr('class', function (d) {
          return 'node'
            + ((d.children || d._children) ? ' node--internal' : ' node--leaf')
            + (d.children ? ' node--expanded' : '')
            + (d._children ? ' node--collapsed' : '')
            + ' ' + d.data.classes
            + (d.data.children_loaded ? ' node--children-loaded' : '')
            ;
        })
        .attr('transform', function (d) {
          if ('radial' == renderer.layout) {
            return "translate(" + radialPoint(getXCoordinate(root_node, true), getYCoordinate(root_node, true)) + ")";
          }
          else {
            return "translate(" + getXCoordinate(root_node, true) + "," + getYCoordinate(root_node, true) + ")";
          }
        })
        .on('click', clickNode);

      var circle = node_enter.append('circle')
        .attr('r', 1e-6)
        // Add tootlip.
        .append("title")
        .text(function(d, i) {
            return d.data.tooltip;
        });

      // Label.
      var label = node_enter.append('text')
        .text(function (d) { return d.data.name; });
      if ('vtree' == renderer.layout) {
        label
        .attr('dx', 3)
        .attr('y', function (d) { return d.children ? -12 : 12; })
        .style('text-anchor', 'middle');
      }
      else {
        label
        .attr('dy', 3)
        .attr('x', function (d) { return d.children ? -12 : 12; })
        .style('text-anchor', function (d) { return d.children ? 'end' : 'start'; });
      }

      // Update.
      var node_update = node_enter.merge(node);

      // Transition to the proper position for the node.
      node_update.transition()
        .duration(duration)
        .attr('transform', function(d) {
          if ('radial' == renderer.layout) {
            return "translate(" + radialPoint(getXCoordinate(d), getYCoordinate(d)) + ")";
          }
          else {
            return 'translate(' + getXCoordinate(d) + ',' + getYCoordinate(d) + ')';
          }
        })
        // Keeps track of the transition to avoid transition collision.
        .on('start', function(d) {
          d.on_transition = true;
        })
        .on('end', function(d) {
          d.on_transition = false;
        });

      // Update the node attributes and style.
      node_update.select('.node circle')
        .attr('r', 5)
        .style('fill', function(d) {
            return d._children ? '#0f0' : '#800';
        });

      // Remove any exiting nodes.
      var node_exit = node.exit().transition()
          .duration(duration)
          .attr('transform', function(d) {
            if ('radial' == renderer.layout) {
              return "translate(" + radialPoint(getXCoordinate(root_node), getYCoordinate(root_node)) + ")";
            }
            else {
              return 'translate(' + getXCoordinate(root_node) + ',' + getYCoordinate(root_node) + ')';
            }
          })
          .remove();

      // On exit reduce the node circles size to 0.
      node_exit.select('circle')
        .attr('r', 1e-6);

      // On exit reduce the opacity of text labels.
      node_exit.select('text')
        .style('fill-opacity', 1e-6);

      // Links.
      var link = g.selectAll('.link')
        .data(tree_nodes.links());

      var link_shape_function,
          link_function,
          transition_link_function,
          remove_link_function;

      // Link rendering functions.
      function linkCurvedHorizontal(s, t) {
        return d3.linkHorizontal()
          .x(function (d) { return d.y; })
          .y(function (d) { return d.x; })
          ({'source': s, 'target': t});
      }

      function linkCurvedVertical(s, t) {
        return d3.linkVertical()
          .x(function (d) { return d.x; })
          .y(function (d) { return d.y; })
          ({'source': s, 'target': t});
      }

      function linkCurvedRadial(s, t) {
        return d3.linkRadial()
          .angle(function(d) { return d.x; })
          .radius(function(d) { return d.y; })
          ({'source': s, 'target': t});
      }

      function linkSquareHorizontal(s, t) {
        return 'M ' + getXCoordinate(s) + ' ' + getYCoordinate(s)
          + "\nL " + getXCoordinate(t) + ' ' + getYCoordinate(s)
          + "\nL " + getXCoordinate(t) + ' ' + getYCoordinate(t);
      }

      function linkSquareVertical(s, t) {
        return 'M ' + getXCoordinate(s) + ' ' + getYCoordinate(s)
          + "\nL " + getXCoordinate(s) + ' ' + getYCoordinate(t)
          + "\nL " + getXCoordinate(t) + ' ' + getYCoordinate(t);
      }

      function linkSquareRadial(s, t) {
        var p =  radialPoint(getXCoordinate(s), getYCoordinate(s));
        var radius = Math.sqrt(p[0]*p[0]+p[1]*p[1]);
        return 'M ' + radialPoint(getXCoordinate(t), getYCoordinate(t)).join(' ')
          // @code A rx ry x-axis-rotation large-arc-flag sweep-flag x y @endcode
          + "\nA " + radius + ' ' + radius + ' 0 0 ' + (getXCoordinate(t) < getXCoordinate(s) ? '1 ' : '0 ') + radialPoint(s.x, t.y).join(' ')
          + "\nL " + radialPoint(getXCoordinate(s), getYCoordinate(s)).join(' ')
      }

      function linkStraight(s, t) {
        if ('radial' == renderer.layout) {
          return 'M ' + radialPoint(s.x, s.y).join(' ') + "\nL " + radialPoint(t.x, t.y).join(' ');
        }
        else {
          return 'M ' + getXCoordinate(s) + ' ' + getYCoordinate(s) + "\nL " + getXCoordinate(t) + ' ' + getYCoordinate(t);
        }
      }

      link_shape_function = linkCurvedHorizontal;
      if ('curved' == renderer.branch_type) {
        if ('vtree' == renderer.layout) {
          link_shape_function = linkCurvedVertical;
        }
        else if ('radial' == renderer.layout) {
          link_shape_function = linkCurvedRadial;
        }
        else {
          link_shape_function = linkCurvedHorizontal;
        }
      }
      else if ('square' == renderer.branch_type) {
        if ('vtree' == renderer.layout) {
          link_shape_function = linkSquareVertical;
        }
        else if ('radial' == renderer.layout) {
          link_shape_function = linkSquareRadial;
        }
        else {
          link_shape_function = linkSquareHorizontal;
        }
      }
      else if ('straight' == renderer.branch_type) {
        link_shape_function = linkStraight;
      }
      else {
        console.log('Invalid tree shape (' + renderer.branch_type + ').');
      }

      link_function = function(d) {
        var point = {'x': root_node.x0, 'y': root_node.y0, 'data': root_node.data};
        return link_shape_function(point, point);
      };
      transition_link_function = function(d) {
        var point1 = {'x': d.target.x, 'y': d.target.y, 'data': d.target.data};
        var point2 = {'x': d.source.x, 'y': d.source.y, 'data': d.source.data};
        return link_shape_function(point1, point2);
      };
      remove_link_function = function(d) {
        var point = {'x': root_node.x, 'y': root_node.y, 'data': root_node.data};
        return link_shape_function(point, point);
      };

      var link_enter = link.enter().insert('path', 'g')
          .attr('class', 'link')
          .attr("d", link_function);

      // Update.
      var link_update = link_enter.merge(link);

      // Transition back to the parent element position.
      link_update.transition()
          .duration(duration)
          .attr('d', transition_link_function);

      // Remove any exiting links
      var link_exit = link.exit().transition()
          .duration(duration)
          .attr('d', remove_link_function)
          .remove();

      // Store the old positions for transitions.
      nodes.forEach(function(d){
        d.x0 = d.x;
        d.y0 = d.y;
      });
    }

    
    
    
    
    
    
    if ('vtree' == renderer.layout) {
      g.attr(
        "transform",
        'translate(0,' + display_margin + ')'
      );
    }
    else if ('htree' == renderer.layout) {
      g.attr(
        "transform",
        'translate(' + display_margin + ',0)'
      );
    }
    else if ('radial' == renderer.layout) {
      g.attr(
        "transform",
        'translate(' + (viewer_height/2) + ',' + (viewer_width/2) + ')'
      );
    }

    // D3 tree layout.
    if ('radial' == renderer.layout) {
      if ('dendogram' == renderer.branch_length) {
        tree = d3.cluster()
          .size([2 * Math.PI, Math.max(200, Math.min(viewer_width, viewer_height))/2])
          .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });
      }
      else {
        tree = d3.tree()
          .size([2 * Math.PI, Math.max(200, Math.min(viewer_width, viewer_height))/2])
          .separation(function(a, b) { return (a.parent == b.parent ? 1 : 2) / a.depth; });
      }
    }
    else {
      if ('dendogram' == renderer.branch_length) {
        if ('vtree' == renderer.layout) {
          tree = d3.cluster()
            .size([viewer_width, viewer_height - 160]);
        }
        else {
          tree = d3.cluster()
            .size([viewer_height, viewer_width - 160]);
        }
      }
      else {
        if ('vtree' == renderer.layout) {
          tree = d3.tree()
            .size([viewer_width, viewer_height - 160]);
        }
        else {
          tree = d3.tree()
            .size([viewer_height, viewer_width - 160]);
        }
      }
    }

    // Manages zooming and panning events.
    svg.call(zoom_listener);

    // Get tree.
    Drupal.behaviors.graphs.getGraph(
      graph_index,
      graph.sourcer,
      {
        graph_id: graph.id,
        node_id: (renderer.root && renderer.root.trim() ? renderer.root : null),
        depth: (renderer.default_depth ? renderer.default_depth : 1),
        rel_types: (renderer.parent_of_relationships ? renderer.parent_of_relationships : ['parent_of']),
        back_rel_types: (renderer.child_of_relationships ? renderer.child_of_relationships : ['child_of']),
        callback: function (graph_data) {
            // Initializes tree data.
            tree_data = convertSourceToTree(graph_data);
            // @todo: use a method to center tree.
            if ('vtree' == renderer.layout) {
              tree_data.x0 = viewer_width / 2;
              tree_data.y0 = 0;
            }
            else if ('htree' == renderer.layout) {
              tree_data.x0 = viewer_height / 2;
              tree_data.y0 = 0;
            }
            else if ('radial' == renderer.layout) {
              tree_data.x0 = viewer_width / 2;
              tree_data.y0 = viewer_height / 2;
            }
            // Renders tree.
            updateSubTree(tree_data);
        }
      }
    );

  }
};

}(jQuery));
